import numpy as np
from convnet import *
from gpumodel import *

class Layer:

    def __init__(self, data, index):
        self.weights = data['model_state']['layers'][index]['weights'][0]
        self.biases = data['model_state']['layers'][index]['biases']

    def writeToFile(self, filename):
        np.savetxt(filename+'_weights.txt', self.weights)
        np.savetxt(filename+'_biases.txt', self.biases)

def main():
    op = ConvNet.get_options_parser()
    op, load_dic = IGPUModel.parse_options(op)

    fst_layer = Layer(data=load_dic, index=2)
    fst_layer.writeToFile('trained_conv1')

    snd_layer = Layer(data=load_dic, index=5)
    snd_layer.writeToFile('trained_conv2')

    trd_layer = Layer(data=load_dic, index=8)
    trd_layer.writeToFile('trained_conv3')

if __name__ == "__main__":
    main()