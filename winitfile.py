import numpy as np

def load_conv1_weights(name, idx, shape, params=None):
    input = np.loadtxt('/home/dmitry/cuda-convnet-master/trained_conv1_weights.txt')
    return np.array(input, dtype=np.single)

def load_conv1_biases(name, shape, params=None):
    input = np.loadtxt('/home/dmitry/cuda-convnet-master/trained_conv1_biases.txt')
    return np.array(input.reshape((32,1)), dtype=np.single)


def load_conv2_weights(name, idx, shape, params=None):
    input = np.loadtxt('/home/dmitry/cuda-convnet-master/trained_conv2_weights.txt')
    return np.array(input, dtype=np.single)

def load_conv2_biases(name, shape, params=None):
    input = np.loadtxt('/home/dmitry/cuda-convnet-master/trained_conv2_biases.txt')
    return np.array(input.reshape((32,1)), dtype=np.single)


def load_conv3_weights(name, idx, shape, params=None):
    input = np.loadtxt('/home/dmitry/cuda-convnet-master/trained_conv3_weights.txt')
    return np.array(input, dtype=np.single)

def load_conv3_biases(name, shape, params=None):
    input = np.loadtxt('/home/dmitry/cuda-convnet-master/trained_conv3_biases.txt')
    return np.array(input.reshape((64,1)), dtype=np.single)